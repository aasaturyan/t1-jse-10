package ru.t1.aasaturyan.tm.model;

public final class Command {

    private String name;

    private String description;

    public Command() {
    }

    public Command(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.isEmpty())
            result += name;
        if (description != null && !description.isEmpty())
            result += ": " + description;
        return result;
    }

}
