package ru.t1.aasaturyan.tm.component;

import ru.t1.aasaturyan.tm.api.*;
import ru.t1.aasaturyan.tm.constant.ArgumentConst;
import ru.t1.aasaturyan.tm.constant.CommandConst;
import ru.t1.aasaturyan.tm.controller.ArgumentController;
import ru.t1.aasaturyan.tm.controller.CommandController;
import ru.t1.aasaturyan.tm.controller.ProjectController;
import ru.t1.aasaturyan.tm.controller.TaskController;
import ru.t1.aasaturyan.tm.repository.ArgumentRepository;
import ru.t1.aasaturyan.tm.repository.CommandRepository;
import ru.t1.aasaturyan.tm.repository.ProjectRepository;
import ru.t1.aasaturyan.tm.repository.TaskRepository;
import ru.t1.aasaturyan.tm.service.ArgumentService;
import ru.t1.aasaturyan.tm.service.CommandService;
import ru.t1.aasaturyan.tm.service.ProjectService;
import ru.t1.aasaturyan.tm.service.TaskService;
import ru.t1.aasaturyan.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IArgumentRepository argumentRepository = new ArgumentRepository();

    private final IArgumentService argumentService = new ArgumentService(argumentRepository);

    private final IArgumentController argumentController = new ArgumentController(argumentService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    public void run(String[] args) {
        processArguments(args);
        processCommands();
    }

    public void processArguments(String[] args) {
        if (args == null || args.length == 0) return;
        final String argument = args[0];
        processArgument(argument);
    }

    public void processCommands() {
        System.out.println("** WELCOME TO TASK MANAGER **");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND: ");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    private void processCommand(String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case CommandConst.HELP:
                commandController.showHelpCommand();
                break;
            case CommandConst.VERSION:
                commandController.showVersion();
                break;
            case CommandConst.ABOUT:
                commandController.showAbout();
                break;
            case CommandConst.EXIT:
                commandController.showExit();
                break;
            case CommandConst.INFO:
                commandController.showSystemInfo();
                break;
            case CommandConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case CommandConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CommandConst.PROJECT_CREATE:
                projectController.createProjects();
                break;
            case CommandConst.TASK_LIST:
                taskController.showTasks();
                break;
            case CommandConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case CommandConst.TASK_CREATE:
                taskController.createTask();
                break;
            default:
                commandController.showErrorCommand();
                break;
        }
    }

    private void processArgument(String argument) {
        if (argument == null || argument.isEmpty()) return;
        switch (argument) {
            case ArgumentConst.HELP:
                argumentController.showHelpArg();
                break;
            case ArgumentConst.VERSION:
                argumentController.showVersion();
                break;
            case ArgumentConst.ABOUT:
                argumentController.showAbout();
                break;
            case ArgumentConst.INFO:
                argumentController.showSystemInfo();
                break;
            default:
                argumentController.showErrorArg();
                break;
        }
        System.exit(0);
    }

}
