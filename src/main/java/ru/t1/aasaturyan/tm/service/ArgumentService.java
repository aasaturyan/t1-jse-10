package ru.t1.aasaturyan.tm.service;

import ru.t1.aasaturyan.tm.api.IArgumentRepository;
import ru.t1.aasaturyan.tm.api.IArgumentService;
import ru.t1.aasaturyan.tm.model.Argument;

public final class ArgumentService implements IArgumentService {

    private final IArgumentRepository argumentRepository;

    public ArgumentService(IArgumentRepository argumentRepository) {
        this.argumentRepository = argumentRepository;
    }

    @Override
    public Argument[] getArguments() {
        return argumentRepository.getArguments();
    }
}
