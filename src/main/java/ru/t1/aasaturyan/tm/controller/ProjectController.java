package ru.t1.aasaturyan.tm.controller;

import ru.t1.aasaturyan.tm.api.IProjectController;
import ru.t1.aasaturyan.tm.api.IProjectService;
import ru.t1.aasaturyan.tm.model.Project;
import ru.t1.aasaturyan.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void createProjects() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.create(name, description);
        if (project == null)
            System.out.println("[FAIL]");
        else
            System.out.println("[OK]");
    }

    @Override
    public void showProjects() {
        int index = 1;
        final List<Project> projects = projectService.findAll();
        if (projects.isEmpty())
            System.out.println("Projects not exists!");
        else {
            System.out.println("[SHOW PROJECTS]");
            for (final Project project : projects) {
                System.out.println(index + ". " + project.getName());
                index++;
            }
            System.out.println("[OK]");
        }
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS]");
        projectService.clear();
        System.out.println("[OK]");
    }


}
