package ru.t1.aasaturyan.tm.controller;

import ru.t1.aasaturyan.tm.api.ICommandController;
import ru.t1.aasaturyan.tm.api.ICommandService;
import ru.t1.aasaturyan.tm.model.Command;
import ru.t1.aasaturyan.tm.util.FormatUtil;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showSystemInfo() {
        final Runtime runtime = Runtime.getRuntime();
        final long availableProcessors = runtime.availableProcessors();
        final long freeMemory = runtime.freeMemory();
        final String freeMemoryFormat = FormatUtil.format(freeMemory);
        final long maxMemory = runtime.maxMemory();
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = maxMemoryCheck ? "no limit" : FormatUtil.format(maxMemory);
        final long totalMemory = runtime.totalMemory();
        final String totalMemoryFormat = FormatUtil.format(totalMemory);
        final long usageMemory = totalMemory - freeMemory;
        final String usageMemoryFormat = FormatUtil.format(usageMemory);

        System.out.println("Available processors (cores): " + availableProcessors);
        System.out.println("Usage memory: " + usageMemoryFormat);
        System.out.println("Maximum memory: " + maxMemoryFormat);
        System.out.println("Free memory: " + freeMemoryFormat);
        System.out.println("Total memory: " + totalMemoryFormat);
    }

    @Override
    public void showExit() {
        System.out.println("[EXIT]");
        System.exit(0);
    }

    @Override
    public void showErrorCommand() {
        System.err.println("[ERROR]");
        System.err.println("This command is not supported...");
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Artyom Asaturyan");
        System.out.println("email: aasaturyan@tech-code.ru");
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.10.0");
    }

    @Override
    public void showHelpCommand() {
        System.out.println("[HELP]");
        for (final Command command : commandService.getCommands())
            System.out.println(command);
    }

}
