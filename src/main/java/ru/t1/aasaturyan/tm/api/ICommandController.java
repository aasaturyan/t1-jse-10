package ru.t1.aasaturyan.tm.api;

public interface ICommandController {
    void showSystemInfo();

    void showExit();

    void showErrorCommand();

    void showAbout();

    void showVersion();

    void showHelpCommand();
}
