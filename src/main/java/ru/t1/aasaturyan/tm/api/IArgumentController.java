package ru.t1.aasaturyan.tm.api;

public interface IArgumentController {

    void showSystemInfo();

    void showErrorArg();

    void showAbout();

    void showVersion();

    void showHelpArg();

}
