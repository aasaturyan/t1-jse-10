package ru.t1.aasaturyan.tm.api;

public interface ITaskController {

    void createTask();

    void showTasks();

    void clearTasks();

}
