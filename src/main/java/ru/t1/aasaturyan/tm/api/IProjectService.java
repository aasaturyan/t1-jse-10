package ru.t1.aasaturyan.tm.api;

import ru.t1.aasaturyan.tm.model.Project;

import java.util.List;

public interface IProjectService {

    Project create(String name, String description);

    Project add(Project project);

    List<Project> findAll();

    void clear();

}
