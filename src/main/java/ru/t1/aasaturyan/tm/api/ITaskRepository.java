package ru.t1.aasaturyan.tm.api;

import ru.t1.aasaturyan.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task add();

    Task add(Task task);

    List<Task> findAll();

    void clear();

}
