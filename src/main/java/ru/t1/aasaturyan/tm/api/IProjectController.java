package ru.t1.aasaturyan.tm.api;

public interface IProjectController {

    void createProjects();

    void showProjects();

    void clearProjects();

}
