package ru.t1.aasaturyan.tm.api;

import ru.t1.aasaturyan.tm.model.Command;

public interface ICommandService {

    Command[] getCommands();

}
