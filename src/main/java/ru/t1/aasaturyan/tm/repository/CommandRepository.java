package ru.t1.aasaturyan.tm.repository;

import ru.t1.aasaturyan.tm.api.ICommandRepository;
import ru.t1.aasaturyan.tm.constant.CommandConst;
import ru.t1.aasaturyan.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command INFO = new Command(
            CommandConst.INFO,
            "Show system info."
    );

    private static final Command ABOUT = new Command(
            CommandConst.ABOUT,
            "Show developer info."
    );

    private static final Command HELP = new Command(
            CommandConst.HELP,
            "Show command list"
    );

    private static final Command VERSION = new Command(
            CommandConst.VERSION,
            "Show application version."
    );

    private static final Command EXIT = new Command(
            CommandConst.EXIT,
            "Close application."
    );

    private static final Command PROJECT_LIST = new Command(
            CommandConst.PROJECT_LIST,
            "Show projects list."
    );

    private static final Command PROJECT_CREATE = new Command(
            CommandConst.PROJECT_CREATE,
            "Create project."
    );

    private static final Command PROJECT_CLEAR = new Command(
            CommandConst.PROJECT_CLEAR,
            "Delete all projects."
    );

    private static final Command TASK_LIST = new Command(
            CommandConst.TASK_LIST,
            "Show tasks list."
    );

    private static final Command TASK_CLEAR = new Command(
            CommandConst.TASK_CLEAR,
            "Delete all tasks."
    );

    private static final Command TASK_CREATE = new Command(
            CommandConst.TASK_CREATE,
            "Create task."
    );

    public static final Command[] COMMANDS = new Command[]{
            ABOUT, INFO, VERSION, HELP, EXIT, PROJECT_LIST, PROJECT_CREATE, PROJECT_CLEAR, TASK_LIST, TASK_CREATE, TASK_CLEAR
    };

    public Command[] getCommands() {
        return COMMANDS;
    }

}
