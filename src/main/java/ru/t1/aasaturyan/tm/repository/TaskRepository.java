package ru.t1.aasaturyan.tm.repository;

import ru.t1.aasaturyan.tm.api.ITaskRepository;
import ru.t1.aasaturyan.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public Task add() {
        return add(null);
    }

    @Override
    public Task add(final Task task) {
        tasks.add(task);
        return task;
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

}
